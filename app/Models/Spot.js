'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Spot extends Model {
    users() {
        return this.belongsToMany('App/Models/Users').pivotTable('user_spots');
    }
    floor() {
        return this.belongsTo('App/Models/Floor');
    }
}

module.exports = Spot
