'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Floor extends Model {
    spot() {
        return this.hasMany('App/Models/Spot');
    }
}

module.exports = Floor
