'use strict'
const User = use('App/Models/User');
const Role = use('App/Models/Role');
class UserController {
    async getByCriteria({ request, response }) {
        const userInfos = request
        let users;
        if (!userInfos.id && !userInfos.username && !userInfos.email && !userInfos.role_id && !userInfos.password) users = await User.query().with('role').setHidden(['password']).fetch()

        else users= await User.query().with('role').where('id',parseInt(userInfos.id)).orWhere('username',userInfos.username).orWhere('email',userInfos.email).orWhere('role_id',parseInt(userInfos.role_id)).orWhere('password',userInfos.password).setHidden(['password']).fetch()
        
        return response.status(200).json({items:users,error:false})
    }
    async update({ request, response }) {
        const userInfos = request.only(['id', 'username', 'role_id' ,'email'])

        if (userInfos.id) {

            const user = await User.find(userInfos.id)
            if (!user) {
                return response.status(201).json({ message: 'user not found',error:true})
            }
            else {
                if (userInfos.username) user.username = userInfos.username;
                if (userInfos.email) user.email = userInfos.email;
                if (userInfos.role_id) user.role_id = userInfos.role_id;
                await user.save()
                return response.status(200).json({items:user,error:false})
            }
        }
        else return response.status(201).json({ message: 'user id has not setted',error:true })
    }

    async delete({ request, response }) {
        //const userId = request.only(['id'])
        if (request.input('id')) {
            const user = await User.find(request.input('id'));

            if (!user) {
                return response.status(201).json({ message: 'user not found',error:true })
            }
            else {
                await user.delete()
                return response.status(201).json({ message: 'The user has successfully deleted',error:false })
            }
        }
        else return response.status(201).json({ message: 'user id has not setted',error:true })
    }
}

module.exports = UserController
