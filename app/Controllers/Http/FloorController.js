'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with floors
 */
const Floor = use('App/Models/Floor');
class FloorController {
  async create({ request, response }) {
    const floorInfos = request.only(['name'])
    const floors = await Floor.all()
    const floor = new Floor()
    if (floorInfos.name) {
      if (typeof floorInfos.name === "string"){
        if(floors.length){
        if(!floors.some(floor => floor.name == floorInfos.name)){
          floor.name = floorInfos.name;
          await floor.save()
          return response.status(200).json({items:floor,error:false})
        }
        else return response.status(201).json({ message: 'floor with given name already exists',error:true })
      }
      else {
        floor.name = floorInfos.name;
        await floor.save()
        return response.status(200).json({items:floor,error:false})
      }
      }
      else return response.status(201).json({ message: 'name field must be contain string value',error:true })
    }
    else return response.status(201).json({ message: 'name field is required and do not accept empty string',error:true })
  }
  async getAll({ request, response }) {
    /*if (!request.input('id') && !request.input('code') && !request.input('value'))const floors = await Floor.all()*/

    //else floors= await Floor.query().where('id',parseInt(request.input('id'))).orWhere('code',request.input('code')).orWhere('value',request.input('value')).fetch()

    //return response.status(200).json(floors)
      const floors= await Floor.all()
      return response.status(200).json({items:floors,error:false})
  }
  async update({ request, response }) {
    const floorInfos = request.only(['id', 'name'])
    const floors = await Floor.all()
    if (floorInfos.id) {

      const floor = await Floor.find(floorInfos.id)
      if (!floor) {
        return response.status(201).json({ message: 'floor not found',error:true })
      }
      else {
        if (floorInfos.name && typeof floorInfos.name === "string"){
          if(floors.length){ 
          if(!floors.some(floor => floor.name == floorInfos.name)) {
            floor.name = floorInfos.name;
            await floor.save()
            return response.status(200).json({items:floor,error:false})
          }
          else return response.status(201).json({ message: 'floor with given name already exists',error:true })
        }
        else{
          floor.name = floorInfos.name;
          await floor.save()
          return response.status(200).json({items:floor,error:false})
        }
      }
      }
    }
    else return response.status(201).json({ message: 'floor id has not setted' ,error:true})
  }

  async delete({ request, response }) {
    //const userId = request.only(['id'])
    if (request.input('id')) {
      const floor = await Floor.find(request.input('id'));
      const spot= await spot.findBy('floor_id',request.input('id'))
      if(!spot){
      if (!floor) {
        return response.status(201).json({ message: 'floor not found' ,error:true})
      }
      else {
        await floor.delete()
        return response.status(201).json({ message: 'The floor has successfully deleted',error:false })
      }
    }
    else return response.status(201).json({ message: 'cannot delete because this floor has been attached to spot(s)',error:true })
    }
    else return response.status(201).json({ message: 'floor id has not setted',error:true })
  }
}

module.exports = FloorController
