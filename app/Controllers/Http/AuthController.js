'use strict'
const User = use('App/Models/User');
class AuthController {
    async register({request, response}) {
        const username = request.input("username")
        const email = request.input("email")
        const password = request.input("password")
        const roleId = request.input("role_id")
        let emailRegex = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
        const existingUser = await User.query().where('username', username).orWhere('email', email).fetch()
      if(username && email && password && roleId && !existingUser.length){
        let user = new User()
        user.username = username
        user.email = email
        user.password = password
        user.role_id= roleId

        user = await user.save()
        //let accessToken = await auth.generate(user)
    return response.json({"items": user,error:false/*, "access_token": accessToken,error:false*/})
      }
      else{
        if(!username) response.json({"message": 'user username has not setted',error:true})
        if(!email) response.json({"message": 'user email has not setted',error:true})
        if(!password) response.json({"message": 'user password has not setted',error:true})
        if(existingUser.length) response.json({"message": 'user with same username or email already exists',error:true})
      }
}
async login({request, response}) {
    const username = request.input("username")
    const password = request.input("password");
    if(username && password) {
      //if (await auth.attempt(email, password)) {
        let user = await User.query().where('username', username).andWhere('password',password).setHidden(['password']).fetch()
        //let accessToken = await auth.generate(user)
        if (!user.length) return response.json({"items":user, error:false/*"access_token": accessToken,error:false*/})
        else response.json({message: 'You first need to register!',error:true,user:user})
      //}

    }
    else  {
      if(!username)  return response.json({message: 'username has not setted',error:true})
      if(!password)  return response.json({message: 'password has not setted',error:true})
    }
}
}

module.exports = AuthController

