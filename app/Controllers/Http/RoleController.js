'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with roles
 */
const Role = use('App/Models/Role');
const User = use('App/Models/User');
class RoleController {
  async create({ request, response }) {
    const roleInfos = request.only(['code', 'value'])
    const roles = await Role.all()
    const role = new Role()
    if (roleInfos.code && roleInfos.value) {
      if (typeof roleInfos.code === "string" && typeof roleInfos.value === "string") {
        // role.code = roleInfos.code;
        // role.value = roleInfos.value;
        //   await role.save()
        //   return response.status(200).json(role)
        if (roles.length) {
          if (!roles.some(role => role.code == roleInfos.code) && !roles.some(role => role.value == roleInfos.value)) {
            role.code = roleInfos.code;
            role.value = roleInfos.value;
            await role.save()
            return response.status(200).json({items:role,error:false})
          }
          else {
            if (roles.some(role => role.code == roleInfos.code)) return response.status(201).json({ message: 'role with given code already exists',error:true })
            if (roles.some(role => role.value == roleInfos.value)) return response.status(201).json({ message: 'role with given value already exists',error:true })
          }
        }
        else {
          role.code = roleInfos.code;
          role.value = roleInfos.value;
          await role.save()
          return response.status(200).json({items:role,error:false})
        }


      }
      else return response.status(201).json({ message: 'code and value fields must be contain string value',error:true })
    }
    else return response.status(201).json({ message: 'code and value fields are required and do not accept empty string',error:true })
  }
  async getAll({ request, response }) {
    /*if (!request.input('id') && !request.input('code') && !request.input('value'))const roles = await Role.all()*/

    //else roles= await Role.query().where('id',parseInt(request.input('id'))).orWhere('code',request.input('code')).orWhere('value',request.input('value')).fetch()

    //return response.status(200).json(roles)
    const roles = await Role.all()
    return response.status(200).json({items:roles,error:false})
  }
  async update({ request, response }) {
    const roleInfos = request.only(['id', 'code', 'value'])

    if (roleInfos.id) {

      const role = await Role.find(roleInfos.id)
      const roles = await Role.all()
      if (!role) {
        return response.status(201).json({ message: 'role not found',error:true })
      }
      else {
        if (roleInfos.code && typeof roleInfos.code === "string") {
          //role.code = roleInfos.code;
          if (roles.length) {
            if (!roles.some(role => role.code == roleInfos.code)) role.code = roleInfos.code;
            else return response.status(201).json({ message: 'role with given code already exists',error:true })
          }
          else role.code = roleInfos.code;
        }
        if (roleInfos.value && typeof roleInfos.value === "string") {
          //role.value = roleInfos.value;
          if (roles.length) {
            if (!roles.some(role => role.value == roleInfos.value)) role.value = roleInfos.value;
            else return response.status(201).json({ message: 'role with given value already exists',error:true })
          }
          else role.value = roleInfos.value;
        }
        if (roles.length) {
          if (!roles.some(role => role.code == roleInfos.code) || !roles.some(role => role.value == roleInfos.value)) {
            await role.save()
            return response.status(200).json({items:role,error:false})
          }
        }
        else {
          await role.save()
          return response.status(200).json({items:role,error:false})
        }
      }
    }
    else return response.status(201).json({ message: 'role id has not setted',error:true })
  }

  async delete({ request, response }) {
    //const userId = request.only(['id'])
    if (request.input('id')) {
      const role = await Role.find(request.input('id'));
      const user = await User.findBy('role_id',request.input('id'))
      if(!user){
      if (!role) {
        return response.status(201).json({ message: 'role not found' ,error:true})
      }
      else {
        await role.delete()
        return response.status(201).json({ message: 'The role has successfully deleted',error:false })
      }
    }
    else return response.json({ message: 'cannot delete because a user has this role',error:true })
    }
    else return response.status(201).json({ message: 'role id has not setted',error:true })
  }
}

module.exports = RoleController
