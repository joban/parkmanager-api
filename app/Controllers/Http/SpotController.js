'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with spots
 */
const Spot = use('App/Models/Spot');
const User = use('App/Models/User');
const Floor = use('App/Models/Floor');
class SpotController {
  async getByCriteria({ request, response }) {
    const spotInfos = request
    let spots;
    if (!spotInfos.id && !spotInfos.number && !spotInfos.occupancy && !spotInfos.occupied) spots = await Spot.query().fetch()

    else spots= await Spot.query().where('id',parseInt(spotInfos.id)).orWhere('number',spotInfos.number).orWhere('ocuppancy',spotInfos.occupancy).orWhere('occupied',spotInfos.occupied).fetch()
    
    return response.status(200).json({items:spots,error:false})
}
async getAll({ request, response }) {
  /*if (!request.input('id') && !request.input('code') && !request.input('value'))const floors = await Floor.all()*/

  //else floors= await Floor.query().where('id',parseInt(request.input('id'))).orWhere('code',request.input('code')).orWhere('value',request.input('value')).fetch()

  //return response.status(200).json(floors)
    const spots= await Spot.query().with('floor').fetch()
    return response.status(200).json({items:spots,error:false})
}
async create({ request, response }) {
  const spotInfos = request.all()
  const spotWithSameNumber = await Spot.findBy('number',spotInfos.number)
  const existingFloor = await Floor.find(spotInfos.floor_id)
  if(spotInfos.number && !spotWithSameNumber && spotInfos.floor_id && existingFloor){ 
  if (spotWithSameNumber) return response.status(201).json({ message: 'spot with same number found' ,error:true})
  else{
  let spot = new Spot()

        spot.number = spotInfos.number;
        spot.occupancy = 0;
        spot.occupied = false;
        spot.floor_id = spotInfos.floor_id;

        await spot.save()
        return response.status(200).json({items:spot,error:false})
  }
}
else{
  if(!spotInfos.number) return response.status(201).json({ message: 'spot number has not setted',error:true })
  if(spotWithSameNumber) return response.status(201).json({ message: 'spot with the same number already exists',error:true })
  if(!spotInfos.floor_id) return response.status(201).json({ message: 'spot floor_id has not setted',error:true })
  if(!existingFloor) return response.status(201).json({ message: 'floor does not exist',error:true })
}
}
async update({ request, response }) {
    const spotInfos = request.only(['id', 'number', 'occupancy' ,'occupied', 'floor_id'])

    if (spotInfos.id) {

        const spot = await Spot.find(spotInfos.id)
        const spotWithSameNumber = await Spot.findBy('number',spotInfos.number) 
        if (!spot || spotWithSameNumber) {
          if (!spot)   return response.status(201).json({ message: 'spot not found' ,error:true})
          if (spotWithSameNumber)   return response.status(201).json({ message: 'spot with same number found' ,error:true})
        }
        else {
            if (spotInfos.number) spot.number = spotInfos.number;
            if (spotInfos.floor_id) spot.floor_id = spotInfos.floor_id;
            await spot.save()
            return response.status(200).json({items:spot,error:false})
        }
    }
    else return response.status(201).json({ message: 'spot id has not setted',error:true })
}

async delete({ request, response }) {
    //const userId = request.only(['id'])
    if (request.input('id')) {
        const spot = await Spot.find(request.input('id'));

        if (!spot) {
            return response.status(201).json({ message: 'spot not found',error:true })
        }
        else {
            await spot.delete()
            return response.status(201).json({ message: 'The spot has successfully deleted',error:false })
        }
    }
    else return response.status(201).json({ message: 'spot id has not setted',error:true })
}

async assign({ request, response }) {
  //const userId = request.only(['id'])
  if (request.input('user_id') && request.input('id')) {

    let user = await User.find(request.input('user_id'));
    let spot = await Spot.find(request.input('id'))
    spot.occupancy = spot.occupancy + 1;
    spot.occupied = true;
    await spot.save()
    await user.spots().attach([spot.id])

    return response.status(201).json({ message: 'The spot has been successfully assigned',error:false })
  }
  else {
    if (!request.input('id')) return response.status(201).json({ message: 'spot id has not setted',error:true })
    if (!request.input('user_id')) return response.status(201).json({ message: 'user id has not setted',error:true })
  }
}
async unassign({ request, response }) {
  //const userId = request.only(['id'])
  if (request.input('user_id') && request.input('id')) {

    let user = await User.find(request.input('user_id'));
    let spot = await Spot.find(request.input('id'))
    //spot.occupancy = spot.occupancy + 1;
    spot.occupied = false;
    await spot.save()
    await user.spots().detach([spot.id])
    
    return response.status(201).json({ message: 'The spot has been successfully unassigned',error:false })
  }
  else {
    if (!request.input('id')) return response.status(201).json({ message: 'spot id has not setted',error:true })
    if (!request.input('user_id')) return response.status(201).json({ message: 'user id has not setted',error:true })
  }
}
async freespot({ request, response }) {
  //const userId = request.only(['id'])
  if (request.input('floor_id')) {
    let spots = await Spot.query().with('floor').where('floor_id',parseInt(request.input('floor_id'))).andWhere('occupied',false).fetch()
    //spot.occupancy = spot.occupancy + 1;
    
    return response.status(201).json({items:spots,error:false})
  }
  else return response.status(201).json({ message: 'floor id has not setted',error:true })
}

async spotByUser({ request, response }) {
  //const userId = request.only(['id'])
  if (request.input('user_id')) {

    const user = await User.find(request.input('user_id'))

    const spots = await user
      .spots()
      .with('floor')
      .fetch()
    //spot.occupancy = spot.occupancy + 1;
    // spot.occupied = false;
    // await spot.save()
    // await user.spots().detach([spot.id])
    
    return response.status(201).json({items:spots,error:false})
  }
  else return response.status(201).json({ message: 'user id has not setted',error:true })
}
}

module.exports = SpotController
