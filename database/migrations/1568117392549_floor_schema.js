'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorSchema extends Schema {
  up () {
    this.create('floors', (table) => {
      table.increments()
      table.string('name',80).notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('floors')
  }
}

module.exports = FloorSchema
