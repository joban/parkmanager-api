'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SpotSchema extends Schema {
  up () {
    this.create('spots', (table) => {
      table.increments();
      table.integer('number', 80).notNullable();
      table.integer('occupancy', 80).notNullable();
      table.boolean('occupied');
      table.integer('floor_id').unsigned().references('id').inTable('floors');
      table.timestamps()
    })
  }

  down () {
    this.drop('spots')
  }
}

module.exports = SpotSchema
