'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/auth/register', 'AuthController.register')
Route.post('/auth/login', 'AuthController.login')
Route.post('/user/getByCriteria', 'UserController.getByCriteria')
Route.post('/user/update', 'UserController.update')

/* Routes for Role */
Route.post('/role/create', 'RoleController.create');
Route.post('/role/update', 'RoleController.update');
Route.post('/role/delete', 'RoleController.delete');
Route.get('/role/getAll', 'RoleController.getAll');

/* Routes for Floor */
Route.post('/floor/create', 'FloorController.create');
Route.post('/floor/update', 'FloorController.update');
Route.post('/floor/delete', 'FloorController.delete');
Route.get('/floor/getAll', 'FloorController.getAll');

/* Routes for Spot */
Route.post('/spot/create', 'SpotController.create');
Route.post('/spot/update', 'SpotController.update');
Route.post('/spot/delete', 'SpotController.delete');
Route.post('/spot/assign', 'SpotController.assign');
Route.post('/spot/unassign', 'SpotController.unassign');
Route.post('/spot/freespot', 'SpotController.freespot');
Route.post('/spot/spotByUser', 'SpotController.spotByUser');
Route.get('/spot/getAll', 'SpotController.getAll');
